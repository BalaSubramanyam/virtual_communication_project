var app = angular.module('myapp', []);
app.controller('myCtrl', function($scope) {
    $scope.homeContainer    = true;
    $scope.Home = function() {
        $scope.homeContainer    = true;
        $scope.companyContainer     = false;
        $scope.PlansContainer     = false;
        $scope.OverviewContainer    = false;
        $scope.ToolsContainer      = false;
        $scope.ContactContainer   = false;
    }
    $scope.Company = function() {
        $scope.companyContainer   = true;
        $scope.homeContainer        = false;
        $scope.PlansContainer     = false;
        $scope.OverviewContainer    = false;
        $scope.ToolsContainer      = false;
        $scope.ContactContainer   = false;
    }
    $scope.Plans = function() {
        $scope.PlansContainer     = true;
        $scope.homeContainer        = false;
        $scope.companyContainer     = false;
        $scope.OverviewContainer    = false;
        $scope.ToolsContainer      = false;
        $scope.ContactContainer   = false;
    }
    $scope.Overview = function() {
        $scope.OverviewContainer    = true;
        $scope.companyContainer     = false;
        $scope.PlansContainer     = false;
        $scope.homeContainer        = false;
        $scope.ToolsContainer      = false;
        $scope.ContactContainer   = false;
    }
    $scope.Tools = function() {
        $scope.ToolsContainer      = true;
        $scope.companyContainer     = false;
        $scope.PlansContainer     = false;
        $scope.OverviewContainer    = false;
        $scope.homeContainer        = false;
        $scope.ContactContainer   = false;
    }
    $scope.Contact = function() {
        $scope.ContactContainer   = true;
        $scope.companyContainer     = false;
        $scope.PlansContainer     = false;
        $scope.OverviewContainer    = false;
        $scope.ToolsContainer      = false;
        $scope.homeContainer        = false;
    }
    $scope.ClientContainer  = false;
    $scope.ClientDisplay = function() {
        $scope.ClientContainer  =   true;
        $scope.EventContainer  = false;   
    }
    $scope.EventContainer  = true;
    $scope.EventDisplay = function() {
        $scope.EventContainer  =   true;
        $scope.ClientContainer  = false;
    }    
    $scope.ObjectiveContainer  = true;
    $scope.ObjectiveDisplay = function() {
        $scope.ObjectiveContainer  =   true;
        $scope.PointContainer  = false;   
    }
    $scope.PointContainer  = false;
    $scope.PointDisplay = function() {
        $scope.PointContainer  =   true;
        $scope.ObjectiveContainer  = false;
    }
    $scope.PrmarketingContainer  = true;
    $scope.PrmarketingDisplay = function() {
        $scope.PrmarketingContainer  =   true;
        $scope.MediaContainer  = false;   
    }
    $scope.MediaContainer  = false;
    $scope.MediaDisplay = function() {
        $scope.MediaContainer  =   true;
        $scope.PrmarketingContainer  = false;
    }
    
    $scope.corporates = 
    [ 
        "Dabur India Ltd", 
        "Reliance JIO", 
        "Madura Garments (Peter England & Allen Solly)", 
        "Max Fashion India", 
        "Marina Infra Projects", 
        "OTL Cab", 
        "Primo Property", 
        "Riya Manbhari Greens", 
        "HOYA Surgical Optics", 
        "Prestogifts.com", 
        "Phoenix India"
    ];
    $scope.healthFirms = 
    [
        "Manipal Hospitals Bangalore",
        "DS Research Centre",
        "Advatech Healthcare",
        "EUOR Health",
        "Soumen’s Workout"
    ];
    $scope.educationFirms = 
    [ 
        "IDP Education",
        "GLOCAL",
        "FIITJEE",
        "NAME"
    ];
     $scope.tourisms = 
    [ 
        "Abu Dhabi Tourism",
        "Gujarat Tourism"
    ];
      $scope.others = 
    [ 
        "Kalakar Awards",
        "GO Seva",
        "Bharat Nirman Awards" ,
        "Dignitaries/ Celebs/ Doctors"
    ];
});